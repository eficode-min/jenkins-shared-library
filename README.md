# Jenkins Shared Library example

This is an example of a simple Jenkins shared library. It has a single task
defined in `vars\runScriptOnSSHTarget.groovy`.
For more information on how to create shared libraries, see
[the shared libraries documentation](https://www.jenkins.io/doc/book/pipeline/shared-libraries/)

## runScriptOnSSHTarget

This task lets you run a script, that already exists on a remote server over
an SSH connection through a bastion host. To execute the script, create a
jenkinsfile with the following content, and replace the parameters to match
your environment:

```
library 'jenkins-shared-library@master' _

runScriptOnSSHTarget {
    scriptPath = '/full/path/to/script.sh'
    cronSpec = 'H \*/12 \* \* \*'
    agentLabel = 'docker'
    bastionHost = 'bastion.example.com'
    bastionHostUser = 'bastion_username'
    taskHost = 'task.example.com'
    taskHostUser = 'task_username'
    bastionHostCredentialId = 'bastion_private_key_credential_id'
    taskHostCredentialId = 'task_private_key_credential_id'
}
```

### scriptPath

Full path to the script to run on the server. The script must
already be located on the server at the given path.

### cronSpec

A cron string defining when the task should run. See the [Jenkins cron string](https://www.jenkins.io/doc/book/pipeline/syntax/#cron-syntax) documentation.

### agentLabel

The label defining which agents can run the pipeline.

### bastionHost

Hostname of the server the SSH connection will be tunnelled through

### bastionHostUser

Username used to connect to the bastionHost.

### bastionHostCredentialId

A Jenkins credential ID string for a credential containing the SSH key
used for logging into the bastionHost.

### taskHost

Hostname of the host that holds the script that will be executed.

### taskHostUser

Username used to connect to the taskHost.

### taskHostCredentialId

A Jenkins credential ID string for a credential containing the SSH key
used for logging into the taskHost.
