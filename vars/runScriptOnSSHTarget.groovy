def call(body) {
    // evaluate the body block, and collect configuration into the config object
    LinkedHashMap config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    def agentLabel = config.agentLabel
    def cronSpec = config.cronSpec
    def bastionHost = config.bastionHost
    def bastionHostUser = config.bastionHostUser
    def taskHost = config.taskHost
    def taskHostUser = config.taskHostUser
    def bastionHostCredentialId = config.bastionHostCredentialId
    def taskHostCredentialId = config.taskHostCredentialId
    def scriptPath = config.scriptPath

    pipeline {
        agent {
            label agentLabel
        }
        // See: https://www.jenkins.io/doc/book/pipeline/syntax/#triggers
        triggers {
            cron(cronSpec)
        }

        stages {
            stage('Execute') {
                steps {
                    sshagent([bastionHostCredentialId, taskHostCredentialId]) {
                        sh """
                            ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -A ${bastionHostUser}@${bastionHost} ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -A ${taskHostUser}@${taskHost} ${scriptPath}
                        """
                    }
                }
            }
        }
    }
}